/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    screens: {
      sm: '768px',
      md: '960px',
      lg: '1220px',
    },
    extend: {
      colors: {
        primary: 'rgba(var(--primary), var(--tw-bg-opacity, 1))',
        secondary: 'rgba(var(--secondary), var(--tw-bg-opacity, 1))',
      }
    },
  },
  plugins: [],
}
