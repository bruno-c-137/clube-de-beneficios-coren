import { Route, BrowserRouter as Router, Routes, Navigate } from "react-router-dom";
import { lazy } from "react";
import { ROUTES } from "../constants/routes";
import LayoutBase from "../layout/base";
const Home = lazy(() => import('../pages/home'))
export default function RoutesLayout() {
    return (
        <Router>
            <LayoutBase>
                <Routes>
                    <Route path={ROUTES.HOME()} element={<Home />} />
                </Routes>
            </LayoutBase>
        </Router>
    );
}