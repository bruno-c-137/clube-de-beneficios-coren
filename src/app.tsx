
import { lazy } from 'react';
const Routes = lazy(() => import('./routesLayout'));

export default function App() {
  return (
    <Routes />
  )
}