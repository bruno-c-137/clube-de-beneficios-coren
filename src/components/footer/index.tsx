import Container from '../container'
import { ReactComponent as Logo } from '../../assets/imgs/logo-footer.svg'
import { ReactComponent as LogoCoren } from '../../assets/imgs/logo-coren.svg'
export default function Footer() {
    return (
        <div className="footer">
            <Container>
                <div className='flex justify-between py-10 border-t border-[#C4C4C4]'>
                    <div className='flex items-center '>
                        <div className='pr-16'>
                            <Logo />
                        </div>
                        <div>
                            <LogoCoren />
                        </div>
                    </div>
                    <div className='hidden md:flex md:items-center'>
                        <div className='flex'>
                            <button className='text-sm text-[#656565]  md:mr-7 lg:mr-10 font-medium'>
                                Início
                            </button>
                            <button className='text-sm text-[#656565]  md:mr-7 lg:mr-10 font-medium'>
                                Como participar
                            </button>
                            <button className='text-sm  text-[#656565]  md:mr-7 lg:mr-10 font-medium'>
                                Parceiros
                            </button>
                            <button className='text-sm  text-[#656565] md:mr-7 lg:mr-10 font-medium'>
                                Como ser um parceiro
                            </button>
                            <button className='text-sm text-[#656565]  md:mr-7 lg:mr-10 font-medium'>
                                Aqui tem o Clube
                            </button>
                            <button className='text-sm text-[#656565] font-medium '>
                                Contato
                            </button>
                        </div>
                    </div>
                </div>
            </Container>
        </div>
    )
}