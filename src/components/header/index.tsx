import Container from '../container'
import { ReactComponent as Logo } from '../../assets/imgs/logo-clube.svg'
export default function Header() {
    return (
        <div className="header">
            <div className='py-5'>
                <Container>
                    <div className='flex justify-between  items-center'>
                        <div className='md:mr-7 lg:mr-16 z-20'>
                            <Logo />
                        </div>
                        <div className='hidden md:flex md:items-center'>
                            <div className=' flex'>
                                <button className='text-sm text-[#656565]  md:mr-7 lg:mr-10 font-medium'>
                                    Início
                                </button>
                                <button className='text-sm text-[#656565]  md:mr-7 lg:mr-10 font-medium'>
                                    Como participar
                                </button>
                                <button className='text-sm  text-[#656565]  md:mr-7 lg:mr-10 font-medium'>
                                    Parceiros
                                </button>
                                <button className='text-sm  text-[#656565] md:mr-7 lg:mr-10 font-medium'>
                                    Como ser um parceiro
                                </button>
                                <button className='text-sm text-[#656565]  md:mr-7 lg:mr-10 font-medium'>
                                    Aqui tem o Clube
                                </button>
                                <button className='text-sm text-[#656565] font-medium '>
                                    Contato
                                </button>
                            </div>
                            {/* <div className='flex pl-10 border-l border-[#C4C4C4]'>
                                <button className='text-sm md:mr-7 lg:mr-10 text-primary'>
                                    <strong>Login</strong>
                                </button>
                                <div>
                                    <button className='text-xs text-white uppercase px-3 py-2 bg-secondary rounded h-full'>
                                        <strong>Cadastrar</strong>
                                    </button>
                                </div>
                            </div> */}
                        </div>
                        {/* <div className={`pl-3 md:hidden z-20 `}  >
                                <div className="flex  ">
                                    <button className='bg-secondary p-4 rounded'>
                                        <span className="hamburguer" ></span>
                                    </button>

                                </div>
                            </div> */}
                    </div>
                </Container>
            </div>
        </div>
    )
}