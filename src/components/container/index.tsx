export default function Container(props: any) {
    return (
        <div className="container mx-auto px-4 md:px-0">{props.children}</div>
    )
} 