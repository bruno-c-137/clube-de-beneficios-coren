import './assets/sass/main.scss'
import React, { Suspense } from 'react'
import ReactDOM from 'react-dom/client'
import App from './app'
import Splash from './components/splash'

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <Suspense fallback={<Splash />}>
    <App />
  </Suspense>,
)
