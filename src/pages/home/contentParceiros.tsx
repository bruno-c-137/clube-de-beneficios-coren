
import LogoEnvolve from '../../assets/imgs/envolve-logo.png'
import LogoFormula from '../../assets/imgs/formula-logo.png'
import LogoSmartFit from '../../assets/imgs/smart-fit-logo.png'
import LogoPremier from '../../assets/imgs/premier-logo.png'
export const ContentParceiros: any = [
    {
        title: 'Farmácias',
        imgs: [LogoSmartFit, LogoFormula, LogoEnvolve, LogoPremier]
    },
    {
        title: 'Supermercados ',
        imgs: [LogoSmartFit, LogoFormula, LogoEnvolve, LogoPremier]
    },
    {
        title: 'Bares e Restaurantes',
        imgs: [LogoSmartFit, LogoFormula, LogoEnvolve, LogoPremier]
    },
    {
        title: 'Academias',
        imgs: [LogoSmartFit, LogoFormula, LogoEnvolve, LogoPremier]
    },
    {
        title: 'Advogados',
        imgs: [LogoSmartFit, LogoFormula, LogoEnvolve, LogoPremier]
    },
    {
        title: 'Roupas e Calçados',
        imgs: [LogoSmartFit, LogoFormula, LogoEnvolve, LogoPremier]
    },
    {
        title: 'Postos de combustível',
        imgs: [LogoEnvolve, LogoSmartFit, LogoFormula, LogoPremier]
    },
    {
        title: 'Material de construção',
        imgs: [LogoSmartFit, LogoFormula, LogoEnvolve, LogoPremier]
    }
]