import Container from "../../components/container"
import { ReactComponent as BannerJaESeu } from '../../assets/imgs/banner-ja-e-seu.svg';
import { ReactComponent as ImgInputNome } from '../../assets/imgs/input-nome.svg';
import { ReactComponent as ImgInputEmail } from '../../assets/imgs/input-email.svg';
import { ReactComponent as ImgSetaDireita } from '../../assets/imgs/seta-direita.svg';
import ImgParaTodaEnfermeira from '../../assets/imgs/img-para-toda-enfermagem.png'
import BannerMockup from '../../assets/imgs/mockup-placa.png'
import { useState } from "react";
import { ContentParceiros } from "./contentParceiros";
export default function Home() {
    const [btnParceiros, setBtnParceiros] = useState<any>(0)
    return (
        <div className="home">
            <Container>
                <div className="flex mt-7">
                    <div className="pt-14 pb-16 pr-24 w-2/5 mr-10">
                        <p className="text-5xl text-primary font-black pr-5 leading-tight">
                            Agora o
                            profissinal de
                        </p>
                        <p className="text-5xl text-secondary font-black leading-tight">
                            Enfermagem
                        </p>
                        <p className="text-5xl text-primary font-black leading-tight">
                            do Rio de Janeiro tem vantagens!
                        </p>
                        <div className="mt-12">
                            <button className="btn-submit text-white text-4xl bg-primary px-12 py-3 rounded-xl shadow-md font-black">
                                Faça parte!
                            </button>
                        </div>
                    </div>
                    <div className="relative">
                        <div className="absolute">
                            <BannerJaESeu />
                        </div>
                    </div>
                </div>

            </Container>
            <div className="bg-gradient-to-r from-primary to-secondary py-36">
                <Container>
                    <div className="flex">
                        <div className="w-1/3">
                            <p className="text-5xl text-white font-black leading-tight">
                                O que é o Clube de Benefícios Coren RJ?
                            </p>
                        </div>
                        <div className=" w-2/3">
                            <p className="text-lg font-medium text-white pl-12 pr-60">
                                O Clube de Benefícios – Coren RJ, nasceu do desejo da atual gestão de trazer para a comunidade de enfermagem carioca benefícios em forma de descontos em inúmeros estabelecimentos.
                                Esperamos que o Clube de benefícios seja usado sem moderação por todos os profissionais de enfermagem 😊.
                            </p>
                        </div>
                    </div>
                </Container>
            </div>
            <div className="py-36">
                <Container>
                    <div className="flex">
                        <div className="w-1/3">
                            <p className="text-5xl text-primary font-black leading-tight">
                                Como obter benefício?
                            </p>
                            <p className="text-lg font-medium text-[#656565] mt-5">
                                Para obter os benefícios em forma de desconto basta identificar
                                se o estabelecimento esta vinculado ao programa,
                                para isso os estabelecimentos inscritos possuem
                                um adesivo indicando participar do programa .
                            </p>
                        </div>
                        <div>
                            <img src={BannerMockup} alt="" />
                        </div>
                    </div>
                </Container>
            </div>
            <div className="bg-[#F3F3F3]">
                <Container>
                    <div className="w-full flex  justify-center ">
                        <button className="font-semibold text-xl text-white bg-gradient-to-r from-secondary to-primary rounded-3xl px-7 py-3 -mt-6 btn-submit">
                            Benefícios,
                            Vantagens,
                            Serviços e
                            Produtos
                        </button>
                    </div>
                    <div className="mt-20 ">
                        <p className="text-5xl text-primary font-black leading-tight text-center">
                            Parceiros
                        </p>
                        <div className=" flex justify-center mt-7">
                            <div className="w-3/4">
                                <div className="flex justify-center  flex-wrap">
                                    {
                                        ContentParceiros?.map((opt: any, index: any) => {
                                            return (
                                                <button onClick={() => setBtnParceiros(index)}
                                                    key={index} className={`text-lg font-medium  px-5  
                                                ${btnParceiros == index ? 'text-white bg-primary rounded-3xl my-2 py-2' : 'text-[#656565] my-4'}`}>
                                                    {opt?.title}
                                                </button>
                                            )
                                        })
                                    }
                                </div>
                                <div className="flex justify-center mt-10">
                                    <hr className="w-1/2 border-[#C4C4C4]" />

                                </div>
                                <div className="flex mt-20 justify-between px-20">
                                    {
                                        ContentParceiros[btnParceiros]?.imgs?.map((optImg: any, index: any) => {
                                            return (
                                                <img key={index} src={optImg} alt="" />
                                            )
                                        })
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="mt-32">
                        <div className="bg-gradient-to-r from-secondary to-primary shadow-md rounded-2xl px-20 py-14 flex">
                            <div className="w-2/3 pr-40">
                                <p className="text-5xl text-white font-black leading-tight">
                                    Seja um parceiro
                                </p>
                                <p className="text-lg font-medium text-white mt-5">
                                    Ao enviar esse formulário, em breve uma equipe do Coren RJ entrará em contato informando a deferência de parceira e documentos complementares.
                                </p>
                            </div>
                            <div className="flex-1">
                                <form>
                                    <div className="pb-5">
                                        <div className="relative flex items-center ">
                                            <div className="absolute h-2/3 px-3 flex items-center border-r border-[#C4C4C4] pointer-events-none  ">
                                                <ImgInputNome />
                                            </div>
                                            <input required className="w-full bg-white pl-14 pr-2 py-3 text-base text-[#656565] rounded-lg shadow-md"
                                                type="text" placeholder='Nome da empresa' />
                                        </div>
                                    </div>
                                    <div className="pb-5">
                                        <div className="relative flex items-center ">
                                            <div className="absolute h-2/3 px-3 flex items-center border-r border-[#C4C4C4] pointer-events-none  ">
                                                <ImgInputEmail />
                                            </div>
                                            <input required className="w-full bg-white pl-14 pr-2 py-3 text-base text-[#656565] rounded-lg shadow-md"
                                                type="text" placeholder='E-mail' />
                                        </div>
                                    </div>
                                    <div>
                                        <button className="btn-submit  bg-secondary text-white px-12  text-base py-3  w-full rounded-lg shadow-md flex justify-center items-center">
                                            <p className="flex-1">Preencher formulário</p>
                                            <div >
                                                <ImgSetaDireita />
                                            </div>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </Container>
                <div className="mt-28 relative flex items-center ">
                    <div className="absolute w-full">
                        <Container>
                            <p className="text-5xl text-white font-black leading-tight w-1/2">
                                Profissional
                                de Enfermagem
                                do Rio de Janeiro
                                com registro
                                ativo já pode
                                aproveitar!
                            </p>
                        </Container>
                    </div>
                    <img src={ImgParaTodaEnfermeira} className='w-full' alt="" />
                </div>
            </div>
        </div>
    )
}