
import { Suspense } from "react";
import Header from "../components/header";
import Footer from "../components/footer";
import Splash from "../components/splash";
export default function LayoutBase(props: any) {
    return (
        <>
            <div id="header">
                <Header />
            </div>
            <div className="main">
                <Suspense fallback={<Splash />}>
                    {props.children}
                </Suspense>
            </div>
            <Footer />

        </>
    )
}